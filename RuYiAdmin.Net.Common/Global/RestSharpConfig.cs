﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// RestSharp配置类
    /// </summary>
    public class RestSharpConfig
    {
        public int TimeOut { get; set; }
    }
}
