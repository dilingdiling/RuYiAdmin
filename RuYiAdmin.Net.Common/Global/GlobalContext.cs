﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// 全局配置
    /// </summary>
    public class GlobalContext
    {
        /// <summary>
        /// 数据库配置
        /// </summary>
        public static DBConfig DBConfig { get; set; }

        /// <summary>
        /// 系统配置
        /// </summary>
        public static SystemConfig SystemConfig { get; set; }

        /// <summary>
        /// 全局配置
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        /// Redis配置
        /// </summary>
        public static RedisConfig RedisConfig { get; set; }

        /// <summary>
        /// ActiveMQ配置
        /// </summary>
        public static ActiveMQConfig ActiveMQConfig { get; set; }

        /// <summary>
        /// Jwt配置
        /// </summary>
        public static JwtSettings JwtSettings { get; set; }

        /// <summary>
        /// 审计日志设置
        /// </summary>
        public static LogConfig LogConfig { get; set; }

        /// <summary>
        /// Smtp邮件配置
        /// </summary>
        public static MailConfig MailConfig { get; set; }

        /// <summary>
        /// 系统目录配置
        /// </summary>
        public static DirectoryConfig DirectoryConfig { get; set; }

        /// <summary>
        /// 定时任务配置
        /// </summary>
        public static QuartzConfig QuartzConfig { get; set; }

        /// <summary>
        /// RestSharp配置
        /// </summary>
        public static RestSharpConfig RestSharpConfig { get; set; }

        /// <summary>
        /// Polly配置
        /// </summary>
        public static PollyConfig PollyConfig { get; set; }

        /// <summary>
        /// Consul配置
        /// </summary>
        public static ConsulConfig ConsulConfig { get; set; }

        /// <summary>
        /// 系统并发配置
        /// </summary>
        public static ConcurrencyLimiterConfig ConcurrencyLimiterConfig { get; set; }

        /// <summary>
        /// SmartThreadPool配置
        /// </summary>
        public static SmartThreadPoolConfig SmartThreadPoolConfig { get; set; }

        /// <summary>
        /// 系统缓存配置
        /// </summary>
        public static SystemCacheConfig SystemCacheConfig { get; set; }

        /// <summary>
        /// 消息中间件配置
        /// </summary>
        public static MOMConfig MOMConfig { get; set; }

        /// <summary>
        /// RabbitMQ配置
        /// </summary>
        public static RabbitMQConfig RabbitMQConfig { get; set; }

        /// <summary>
        /// 全局路由模板
        /// </summary>
        public const String RouteTemplate = "API/[controller]/[action]";
    }
}
