﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// ActiveMQ配置
    /// </summary>
    public class ActiveMQConfig
    {
        /// <summary>
        /// ActiveMQ连接串
        /// </summary>
        public String ConnectionString { get; set; }

        /// <summary>
        /// 主题名称
        /// </summary>
        public String TopicName { get; set; }

        /// <summary>
        /// 队列名称
        /// </summary>
        public String QueueName { get; set; }

        /// <summary>
        /// 消息名称前缀
        /// </summary>
        public String MessagePrefix { get; set; }
    }
}
