﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// 审计日志设置
    /// </summary>
    public class LogConfig
    {
        /// <summary>
        /// 审计日志是否开启
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 审计日志分表采集年份
        /// </summary>
        public int SplitTableYearTake { get; set; }

        /// <summary>
        /// 是否支持MongoDB
        /// </summary>
        public bool SupportMongoDB { get; set; }

        /// <summary>
        /// MongoDB数据库地址
        /// </summary>
        public String MongoUrl { get; set; }

        /// <summary>
        /// 默认数据库
        /// </summary>
        public String MongoDefaultDB { get; set; }
    }
}
