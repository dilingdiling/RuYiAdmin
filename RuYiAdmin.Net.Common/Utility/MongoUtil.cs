﻿using MongoDB.Driver;
using RuYiAdmin.Net.Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// MongoDB工具类
    /// </summary>
    public class MongoUtil
    {
        /// <summary>
        /// 私有化构造函数
        /// 用于单例模式
        /// </summary>
        private MongoUtil() { }

        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<MongoClient> LazyInstance = new Lazy<MongoClient>(() =>
        {
            if (GlobalContext.LogConfig.SupportMongoDB)
            {
                var client = new MongoClient(GlobalContext.LogConfig.MongoUrl);
                return client;
            }
            return null;
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static MongoClient Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }
    }
}
