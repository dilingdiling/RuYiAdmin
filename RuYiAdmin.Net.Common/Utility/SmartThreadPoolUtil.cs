﻿using Amib.Threading;
using RuYiAdmin.Net.Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// SmartThreadPool工具类
    /// </summary>
    public class SmartThreadPoolUtil
    {

        /// <summary>
        /// 私有化构造函数
        /// 用于单例模式
        /// </summary>
        private SmartThreadPoolUtil() { }

        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<SmartThreadPool> LazyInstance = new Lazy<SmartThreadPool>(() =>
        {
            var stp = new SmartThreadPool(
                GlobalContext.SmartThreadPoolConfig.IdleTimeout,
                GlobalContext.SmartThreadPoolConfig.MaxThreads,
                GlobalContext.SmartThreadPoolConfig.MinThreads
                );
            stp.Name = GlobalContext.SmartThreadPoolConfig.Name;
            //stp.Concurrency = GlobalContext.SmartThreadPoolConfig.Concurrency;
            stp.CreateWorkItemsGroup(GlobalContext.SmartThreadPoolConfig.WorkItemsGroup);
            return stp;
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static SmartThreadPool Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }
    }
}
