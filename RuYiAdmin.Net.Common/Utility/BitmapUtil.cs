﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// Bitmap工具类
    /// </summary>
    public class BitmapUtil
    {
        /// <summary>
        /// ToBase64
        /// </summary>
        /// <param name="bmp">Bitmap</param>
        /// <returns>Base64字符串</returns>
        public static String ToBase64(Bitmap bmp)
        {
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] arr = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(arr, 0, (int)ms.Length);
            ms.Close();
            String strbaser64 = Convert.ToBase64String(arr);
            return strbaser64;
        }

        /// <summary>
        /// ToImage
        /// </summary>
        /// <param name="base64Str">Base64String</param>
        /// <returns>Bitmap</returns>
        public static Bitmap ToImage(string base64Str)
        {
            byte[] arr = Convert.FromBase64String(base64Str);
            MemoryStream ms = new MemoryStream(arr);
            Bitmap bmp = new Bitmap(ms);
            ms.Close();
            return bmp;
        }
    }
}
