﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// 字符串工具类
    /// </summary>
    public class StringUtil
    {
        /// <summary>
        /// 数组字符串转为Guid数组
        /// </summary>
        /// <param name="ids">数组字符串</param>
        /// <returns>Guid数组</returns>
        public static Guid[] GetGuids(String ids)
        {
            if (!String.IsNullOrEmpty(ids))
            {
                var arrStrs = ids.Split(',');

                var arrGuids = new Guid[arrStrs.Length];

                for (var i = 0; i < arrStrs.Length; i++)
                {
                    arrGuids[i] = Guid.Parse(arrStrs[i]);
                }

                return arrGuids;
            }

            return null;
        }

        /// <summary>
        /// 判断是否为空
        /// </summary>
        /// <param name="obj">Object对象</param>
        /// <returns>bool</returns>
        public static bool IsNullOrEmpty(Object obj)
        {
            var result = false;

            if (obj == null)
            {
                result = true;
            }
            else
            {
                if (String.IsNullOrEmpty(obj.ToString()))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// 判断是否包含字符串
        /// </summary>
        /// <param name="container">容器</param>
        /// <param name="obj">Object对象</param>
        /// <returns>bool</returns>
        public static bool IsContains(String[] container, Object obj)
        {
            var result = false;

            if (container.Length > 0 && obj != null)
            {
                if (container.Contains(obj.ToString()))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
