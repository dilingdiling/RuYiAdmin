﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.CommonClass.Excel
{
    /// <summary>
    /// ExcelExport属性
    /// </summary>
    public class ExcelExportAttribute : Attribute
    {
        /// <summary>
        /// 列名
        /// </summary>
        public String ColumnName { get; }

        public ExcelExportAttribute(String columnName)
        {
            this.ColumnName = columnName;
        }
    }
}
