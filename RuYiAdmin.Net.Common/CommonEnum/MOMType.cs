﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.CommonEnum
{
    /// <summary>
    /// 消息中间件类型
    /// </summary>
    public enum MOMType
    {
        /// <summary>
        /// ActiveMQ
        /// </summary>
        ActiveMQ,

        /// <summary>
        /// RabbitMQ
        /// </summary>
        RabbitMQ
    }
}
