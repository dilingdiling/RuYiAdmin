﻿using RuYiAdmin.Net.Common.CommonEnum;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RuYiAdmin.Net.Repository.BusinessRepository.MQ
{
    /// <summary>
    /// ActiveMQ访问层实现
    /// </summary>
    public class MQRepository : IMQRepository
    {
        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public void SendTopic(String message, String topicName = null)
        {
            if (GlobalContext.MOMConfig.MOMType == MOMType.ActiveMQ)
            {
                ActiveMQUtil.SendTopic(message, topicName);
            }
            else if (GlobalContext.MOMConfig.MOMType == MOMType.RabbitMQ)
            {
                RabbitMQUtil.SendMessage(message);
            }
        }

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public async Task SendTopicAsync(String message, String topicName = null)
        {
            if (GlobalContext.MOMConfig.MOMType == MOMType.ActiveMQ)
            {
                await ActiveMQUtil.SendTopicAsync(message, topicName);
            }
            else if (GlobalContext.MOMConfig.MOMType == MOMType.RabbitMQ)
            {
                await RabbitMQUtil.SendMessageAsynce(message);
            }
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public void SendQueue(String message, String queueName = null)
        {
            if (GlobalContext.MOMConfig.MOMType == MOMType.ActiveMQ)
            {
                ActiveMQUtil.SendQueue(message, queueName);
            }
            else if (GlobalContext.MOMConfig.MOMType == MOMType.RabbitMQ)
            {
                RabbitMQUtil.SendMessage(message);
            }
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public async Task SendQueueAsync(String message, String queueName = null)
        {
            if (GlobalContext.MOMConfig.MOMType == MOMType.ActiveMQ)
            {
                await ActiveMQUtil.SendQueueAsync(message, queueName);
            }
            else if (GlobalContext.MOMConfig.MOMType == MOMType.RabbitMQ)
            {
                await RabbitMQUtil.SendMessageAsynce(message);
            }
        }
    }
}
