﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.RoleMenu
{
    /// <summary>
    /// 角色菜单数据访问层接口
    /// </summary>
    public interface IRoleMenuRepository : IRuYiAdminBaseRepository<SysRoleMenu>
    {
    }
}
