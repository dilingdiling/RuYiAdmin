﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.OrgUser
{
    /// <summary>
    /// 机构用户数据访问层接口
    /// </summary>
    public interface IOrgUserRepository : IRuYiAdminBaseRepository<SysOrgUser>
    {
    }
}
