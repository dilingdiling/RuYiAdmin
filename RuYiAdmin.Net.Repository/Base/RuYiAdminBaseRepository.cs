﻿using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Entity.CoreEnum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.Base
{
    /// <summary>
    /// 数据库访问基类
    /// </summary>
    public class RuYiAdminBaseRepository<T> : RuYiAdminDbContext, IRuYiAdminBaseRepository<T> where T : RuYiAdminBaseEntity, new()
    {
        #region 属性及构造函数

        /// <summary>
        /// 数据库上下文
        /// </summary>
        //private readonly Repository<T> Repository;

        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public RuYiAdminBaseRepository(IHttpContextAccessor context)
        {
            //this.Repository = new Repository<T>();
            this.context = context;
        }

        #endregion

        #region 公有方法

        #region 同步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<T> GetPage(QueryCondition queryCondition, ref int totalCount)
        {
            QueryCondition.AddDefaultQueryItem(queryCondition);

            var where = QueryCondition.BuildExpression<T>(queryCondition.QueryItems);

            var list = RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!String.IsNullOrEmpty(queryCondition.Sort), queryCondition.Sort).
                ToPageList(queryCondition.PageIndex, queryCondition.PageSize, ref totalCount).
                ToList();

            return list;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public List<T> GetList(QueryCondition queryCondition)
        {
            QueryCondition.AddDefaultQueryItem(queryCondition);

            var where = QueryCondition.BuildExpression<T>(queryCondition.QueryItems);

            var list = RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!String.IsNullOrEmpty(queryCondition.Sort), queryCondition.Sort).
                ToList();

            return list;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public T GetById(Guid id)
        {
            return RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<T> SqlQuery(QueryCondition queryCondition, String sqlKey, ref int totalCount)
        {
            var sqlStr = this.GetQuerySQL(queryCondition, sqlKey);

            var list = this.GetPageList(queryCondition, sqlStr, ref totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public List<T> SqlQuery(QueryCondition queryCondition, ref int totalCount, String strSQL)
        {
            var baseSQL = this.GetBaseSQL();
            baseSQL = String.Format(baseSQL, strSQL);

            var condition = this.ConvertQueryCondition(queryCondition.QueryItems);
            var sort = this.ConvertSort(queryCondition.Sort);

            var sqlStr = String.Join("", baseSQL, condition, sort);

            var list = this.GetPageList(queryCondition, sqlStr, ref totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        public List<TDTO> SqlQuery<TDTO>(QueryCondition queryCondition, String sqlKey, ref int totalCount) where TDTO : class, new()
        {
            var sqlStr = this.GetQuerySQL(queryCondition, sqlKey);

            var list = this.GetPageList<TDTO>(queryCondition, sqlStr, ref totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public List<TDTO> SqlQuery<TDTO>(QueryCondition queryCondition, ref int totalCount, String strSQL) where TDTO : class, new()
        {
            var baseSQL = this.GetBaseSQL();
            baseSQL = String.Format(baseSQL, strSQL);

            var condition = this.ConvertQueryCondition(queryCondition.QueryItems);
            var sort = this.ConvertSort(queryCondition.Sort);

            var sqlStr = String.Join("", baseSQL, condition, sort);

            var list = this.GetPageList<TDTO>(queryCondition, sqlStr, ref totalCount);

            return list;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        public List<T> GetList()
        {
            return RuYiDbContext.Queryable<T>().ToList();
        }

        /// <summary>
        /// 按表达式查询
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>列表</returns>
        public List<T> QueryByExpression(Expression<Func<T, bool>> expression)
        {
            return RuYiDbContext.Queryable<T>().Where(expression).ToList();
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <returns>对象</returns>
        public T AddEntity(T obj)
        {
            //判断审计日志记录开关是否开启
            if (obj.GetType().Equals(typeof(SysLog)))
            {
                if (!GlobalContext.LogConfig.IsEnabled)
                {
                    return obj;
                }
            }

            obj.Create(this.context);

            RuYiDbContext.Insertable<T>(obj).ExecuteCommand();

            return obj;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>集合</returns>
        public List<T> AddList(List<T> list)
        {
            //判断审计日志记录开关是否开启
            if (list != null && list.Count > 0 && list.FirstOrDefault().GetType().Equals(typeof(SysLog)))
            {
                if (!GlobalContext.LogConfig.IsEnabled)
                {
                    return list;
                }
            }

            foreach (var item in list)
            {
                item.Create(this.context);
            }

            RuYiDbContext.Insertable<T>(list).ExecuteCommand();

            return list;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <returns>对象</returns>
        public T UpdateEntity(T obj)
        {
            var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(obj).ToList().FirstOrDefault();
            if (!(Math.Round(entity.ModifyTime.TimeOfDay.TotalSeconds, 3) == Math.Round(obj.ModifyTime.TimeOfDay.TotalSeconds, 3)))
            {
                throw new VersionExceptions("data is dirty");
            }

            obj.Modify(this.context);
            RuYiDbContext.Updateable<T>(obj).ExecuteCommand();

            return obj;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>集合</returns>
        public List<T> UpdateList(List<T> list)
        {
            foreach (var item in list)
            {
                var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(item).ToList().FirstOrDefault();
                if (!(Math.Round(entity.ModifyTime.TimeOfDay.TotalSeconds, 3) == Math.Round(item.ModifyTime.TimeOfDay.TotalSeconds, 3)))
                {
                    throw new VersionExceptions("data is dirty");
                }

                item.Modify(this.context);
            }

            RuYiDbContext.Updateable<T>(list).ExecuteCommand();

            return list;
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>标志</returns>
        public bool DeleteEntity(Guid id)
        {
            var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));

            entity.Delete(this.context);

            return RuYiDbContext.Updateable<T>(entity).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>标志位</returns>
        public bool DeleteRange(Guid[] ids)
        {
            var list = new List<T>();

            foreach (var item in ids)
            {
                var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(item));

                entity.Delete(this.context);

                list.Add(entity);
            }

            return RuYiDbContext.Updateable<T>(list).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>标志</returns>
        public bool RemoveEntity(Guid id)
        {
            return RuYiDbContext.Deleteable<T>().In(id).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>标志</returns>
        public bool RemoveRange(Guid[] ids)
        {
            return RuYiDbContext.Deleteable<T>().In(ids).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int BulkCopy(List<T> list)
        {
            return RuYiDbContext.Fastest<T>().BulkCopy(list);
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int PageBulkCopy(int pageSize, List<T> list)
        {
            return RuYiDbContext.Fastest<T>().PageSize(pageSize).BulkCopy(list);
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int BulkUpdate(List<T> list)
        {
            return RuYiDbContext.Fastest<T>().BulkUpdate(list);
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int HugeDataBulkCopy(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return hugeData.BulkCopy();
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int HugeDataBulkUpdate(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return hugeData.BulkUpdate();
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<bool> UseTransaction(Action action, Action<Exception> errorCallBack = null)
        {
            return RuYiDbContext.AsTenant().UseTran(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<TEntity> UseTransaction<TEntity>(Func<TEntity> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return RuYiDbContext.AsTenant().UseTran(action, errorCallBack);
        }

        #endregion

        #region 异步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> GetPageAsync(QueryCondition queryCondition, RefAsync<int> totalCount)
        {
            QueryCondition.AddDefaultQueryItem(queryCondition);

            var where = QueryCondition.BuildExpression<T>(queryCondition.QueryItems);

            var list = await RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!String.IsNullOrEmpty(queryCondition.Sort), queryCondition.Sort).
                ToPageListAsync(queryCondition.PageIndex, queryCondition.PageSize, totalCount);

            return list;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> GetListAsync(QueryCondition queryCondition)
        {
            QueryCondition.AddDefaultQueryItem(queryCondition);

            var where = QueryCondition.BuildExpression<T>(queryCondition.QueryItems);

            var list = await RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!String.IsNullOrEmpty(queryCondition.Sort), queryCondition.Sort).
                ToListAsync();

            return list;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await RuYiDbContext.Queryable<T>().FirstAsync(t => t.Id.Equals(id));
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> SqlQueryAsync(QueryCondition queryCondition, String sqlKey, RefAsync<int> totalCount)
        {
            var sqlStr = this.GetQuerySQL(queryCondition, sqlKey);

            var list = await this.GetPageListAsync(queryCondition, sqlStr, totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> SqlQueryAsync(QueryCondition queryCondition, RefAsync<int> totalCount, String strSQL)
        {
            var baseSQL = this.GetBaseSQL();
            baseSQL = String.Format(baseSQL, strSQL);

            var condition = this.ConvertQueryCondition(queryCondition.QueryItems);
            var sort = this.ConvertSort(queryCondition.Sort);

            var sqlStr = String.Join("", baseSQL, condition, sort);

            var list = await this.GetPageListAsync(queryCondition, sqlStr, totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> SqlQueryAsync<TDTO>(QueryCondition queryCondition, String sqlKey, RefAsync<int> totalCount) where TDTO : class, new()
        {
            var sqlStr = this.GetQuerySQL(queryCondition, sqlKey);

            var list = await this.GetPageListAsync<TDTO>(queryCondition, sqlStr, totalCount);

            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> SqlQueryAsync<TDTO>(QueryCondition queryCondition, RefAsync<int> totalCount, String strSQL) where TDTO : class, new()
        {
            var baseSQL = this.GetBaseSQL();
            baseSQL = String.Format(baseSQL, strSQL);

            var condition = this.ConvertQueryCondition(queryCondition.QueryItems);
            var sort = this.ConvertSort(queryCondition.Sort);

            var sqlStr = String.Join("", baseSQL, condition, sort);

            var list = await this.GetPageListAsync<TDTO>(queryCondition, sqlStr, totalCount);

            return list;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        public async Task<List<T>> GetListAsync()
        {
            return await RuYiDbContext.Queryable<T>().ToListAsync();
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <returns>对象</returns>
        public async Task<T> AddEntityAsync(T obj)
        {
            //判断审计日志记录开关是否开启
            if (obj.GetType().Equals(typeof(SysLog)))
            {
                if (!GlobalContext.LogConfig.IsEnabled)
                {
                    return obj;
                }
            }

            obj.Create(this.context);

            await RuYiDbContext.Insertable<T>(obj).ExecuteCommandAsync();

            return obj;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>集合</returns>
        public async Task<List<T>> AddListAsync(List<T> list)
        {
            //判断审计日志记录开关是否开启
            if (list.FirstOrDefault().GetType().Equals(typeof(SysLog)))
            {
                if (!GlobalContext.LogConfig.IsEnabled)
                {
                    return list;
                }
            }

            foreach (var item in list)
            {
                item.Create(this.context);
            }

            await RuYiDbContext.Insertable<T>(list).ExecuteCommandAsync();

            return list;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <returns>对象</returns>
        public async Task<T> UpdateEntityAsync(T obj)
        {
            var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(obj).ToList().FirstOrDefault();
            if (!(Math.Round(entity.ModifyTime.TimeOfDay.TotalSeconds, 3) == Math.Round(obj.ModifyTime.TimeOfDay.TotalSeconds, 3)))
            {
                throw new VersionExceptions("data is dirty");
            }

            obj.Modify(this.context);
            await RuYiDbContext.Updateable<T>(obj).ExecuteCommandAsync();

            return obj;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>集合</returns>
        public async Task<List<T>> UpdateListAsync(List<T> list)
        {
            foreach (var item in list)
            {
                var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(item).ToList().FirstOrDefault();
                if (!(Math.Round(entity.ModifyTime.TimeOfDay.TotalSeconds, 3) == Math.Round(item.ModifyTime.TimeOfDay.TotalSeconds, 3)))
                {
                    throw new VersionExceptions("data is dirty");
                }

                item.Modify(this.context);
            }

            await RuYiDbContext.Updateable<T>(list).ExecuteCommandAsync();

            return list;
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>标志</returns>
        public async Task<bool> DeleteEntityAsync(Guid id)
        {
            var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));

            entity.Delete(this.context);

            return await RuYiDbContext.Updateable<T>(entity).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>标志位</returns>
        public async Task<bool> DeleteRangeAsync(Guid[] ids)
        {
            var list = new List<T>();
            foreach (var item in ids)
            {
                var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(item));

                entity.Delete(this.context);

                list.Add(entity);
            }
            return await RuYiDbContext.Updateable<T>(list).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>标志</returns>
        public async Task<bool> RemoveEntityAsync(Guid id)
        {
            return await RuYiDbContext.Deleteable<T>().In(id).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>标志</returns>
        public async Task<bool> RemoveRangeAsync(Guid[] ids)
        {
            return await RuYiDbContext.Deleteable<T>().In(ids).ExecuteCommandAsync() > 0;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> BulkCopyAsync(List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().BulkCopyAsync(list);
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> PageBulkCopyAsync(int pageSize, List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().PageSize(pageSize).BulkCopyAsync(list);
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> BulkUpdateAsync(List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().BulkUpdateAsync(list);
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> HugeDataBulkCopyAsync(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return await hugeData.BulkCopyAsync();
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> HugeDataBulkUpdateAsync(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return await hugeData.BulkUpdateAsync();
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<bool>> UseTransactionAsync(Func<Task> action, Action<Exception> errorCallBack = null)
        {
            return await RuYiDbContext.AsTenant().UseTranAsync(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<TEntity>> UseTransactionAsync<TEntity>(Func<Task<TEntity>> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return await RuYiDbContext.AsTenant().UseTranAsync(action, errorCallBack);
        }

        #endregion

        #endregion

        #region 私有方法        

        /// <summary>
        /// 获取基本语句
        /// </summary>
        /// <returns></returns>
        private String GetBaseSQL()
        {
            var baseSQL = GlobalContext.Configuration.GetSection("sqls:sql:basequerysql").Value;

            if ((DbType)GlobalContext.DBConfig.DBType == DbType.SqlServer)
            {
                baseSQL = baseSQL.Replace("*", "TOP 100 percent *");
            }

            return baseSQL;
        }

        /// <summary>
        /// 获取查询脚本
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>sql语句</returns>
        private String GetQuerySQL(QueryCondition queryCondition, String sqlKey)
        {
            var baseSQL = this.GetBaseSQL();

            var sqlStr = GlobalContext.Configuration.GetSection(sqlKey).Value;

            baseSQL = String.Format(baseSQL, sqlStr);

            var condition = this.ConvertQueryCondition(queryCondition.QueryItems);

            var sort = this.ConvertSort(queryCondition.Sort);

            return String.Join("", baseSQL, condition, sort);
        }

        /// <summary>
        /// 转化查询条件
        /// </summary>
        /// <param name="queryItems">查询条件</param>
        /// <returns>查询语句</returns>
        private String ConvertQueryCondition(List<QueryItem> queryItems)
        {
            var queryCondition = String.Empty;
            if (queryItems != null && queryItems.Count > 0)
            {
                queryCondition = QueryCondition.ConvertToSQL(queryItems);
            }
            return queryCondition;
        }

        /// <summary>
        /// 转化排序条件
        /// </summary>
        /// <param name="sort">排序条件</param>
        /// <returns>排序语句</returns>
        private String ConvertSort(String sort)
        {
            var sortStr = String.Empty;
            if (!String.IsNullOrEmpty(sort))
            {
                sortStr = " ORDER BY " + sort;
            }
            return sortStr;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="strSQL">查询语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询集合</returns>
        private List<T> GetPageList(QueryCondition queryCondition, String strSQL, ref int totalCount)
        {
            var list = new List<T>();
            if (queryCondition.PageIndex >= 0 && queryCondition.PageSize > 0)
            {
                list = RuYiDbContext.SqlQueryable<T>(strSQL).
                ToPageList(queryCondition.PageIndex, queryCondition.PageSize, ref totalCount);
            }
            else
            {
                list = RuYiDbContext.SqlQueryable<T>(strSQL).ToList();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlStr">查询语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        private List<TDTO> GetPageList<TDTO>(QueryCondition queryCondition, String sqlStr, ref int totalCount) where TDTO : class, new()
        {
            var list = new List<TDTO>();
            if (queryCondition.PageIndex >= 0 && queryCondition.PageSize > 0)
            {
                list = RuYiDbContext.SqlQueryable<TDTO>(sqlStr).
                ToPageList(queryCondition.PageIndex, queryCondition.PageSize, ref totalCount);
            }
            else
            {
                list = RuYiDbContext.SqlQueryable<TDTO>(sqlStr).ToList();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        private async Task<List<T>> GetPageListAsync(QueryCondition queryCondition, String sqlStr, RefAsync<int> totalCount)
        {
            var list = new List<T>();
            if (queryCondition.PageIndex >= 0 && queryCondition.PageSize > 0)
            {
                list = await RuYiDbContext.SqlQueryable<T>(sqlStr).
                ToPageListAsync(queryCondition.PageIndex, queryCondition.PageSize, totalCount);
            }
            else
            {
                list = await RuYiDbContext.SqlQueryable<T>(sqlStr).ToListAsync();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="queryCondition">查询条件</param>
        /// <param name="sqlStr">查询语句</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        private async Task<List<TDTO>> GetPageListAsync<TDTO>(QueryCondition queryCondition, String sqlStr, RefAsync<int> totalCount) where TDTO : class, new()
        {
            var list = new List<TDTO>();
            if (queryCondition.PageIndex >= 0 && queryCondition.PageSize > 0)
            {
                list = await RuYiDbContext.SqlQueryable<TDTO>(sqlStr).
                ToPageListAsync(queryCondition.PageIndex, queryCondition.PageSize, totalCount);
            }
            else
            {
                list = await RuYiDbContext.SqlQueryable<TDTO>(sqlStr).ToListAsync();
                totalCount = list.Count;
            }
            return list;
        }

        #endregion
    }
}
