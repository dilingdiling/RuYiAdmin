﻿using RuYiAdmin.Net.Common.Global;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.Base
{
    /// <summary>
    /// 仓储基类
    /// </summary>
    /// <typeparam name="T">实体类</typeparam>
    //public class Repository<T> : SimpleClient<T> where T : class, new()
    //{
    //    public Repository(ISqlSugarClient context = null) : base(context)//注意这里要有默认值等于null
    //    {
    //        if (context == null)
    //        {
    //            base.Context = new SqlSugarClient(new ConnectionConfig()
    //            {
    //                DbType = (DbType)GlobalContext.DBConfig.DBType,
    //                InitKeyType = InitKeyType.Attribute,
    //                IsAutoCloseConnection = true,
    //                ConnectionString = GlobalContext.DBConfig.ConnectionString,
    //                //读写分离从库，如果使用MyCat搭建读写分离集群，请注释以下从库配置
    //                SlaveConnectionConfigs = new List<SlaveConnectionConfig>() {
    //                 new SlaveConnectionConfig() { HitRate=10, ConnectionString=GlobalContext.DBConfig.SlaveConnectionString } ,
    //                 new SlaveConnectionConfig() { HitRate=10, ConnectionString=GlobalContext.DBConfig.SlaveConnectionString2 }
    //                }
    //            });
    //            //数据库超时时间，单位秒
    //            base.Context.Ado.CommandTimeOut = GlobalContext.DBConfig.CommandTimeOut;
    //        }
    //    }

    //    /// <summary>
    //    /// 扩展方法
    //    /// </summary>
    //    /// <returns></returns>
    //    public List<T> CommQuery(string json)
    //    {
    //        //base.Context.Queryable<T>().ToList();
    //        //可以拿到SqlSugarClient做复杂操作
    //        return null;
    //    }
    //}

    /// <summary>
    /// SqlSugar数据库上下文
    /// </summary>
    public class RuYiAdminDbContext
    {
        /// <summary>
        /// 单例模式对象
        /// </summary>
        public static SqlSugarScope RuYiDbContext = new SqlSugarScope(new ConnectionConfig()
        {
            DbType = (DbType)GlobalContext.DBConfig.DBType,
            InitKeyType = InitKeyType.Attribute,
            IsAutoCloseConnection = true,
            ConnectionString = GlobalContext.DBConfig.ConnectionString,//主库
            MoreSettings = new ConnMoreSettings()
            {
                //MySql禁用NVarchar
                DisableNvarchar = (DbType)GlobalContext.DBConfig.DBType == DbType.MySql ? true : false
            },
            //读写分离从库，如果使用MyCat搭建读写分离集群，请注释以下从库配置
            SlaveConnectionConfigs = new List<SlaveConnectionConfig>() {
                     new SlaveConnectionConfig() { HitRate=10, ConnectionString=GlobalContext.DBConfig.SlaveConnectionString } ,
                     new SlaveConnectionConfig() { HitRate=10, ConnectionString=GlobalContext.DBConfig.SlaveConnectionString2 }
                    }
        },
         db =>
         {
             //单例参数配置，所有上下文生效
             db.Ado.CommandTimeOut = GlobalContext.DBConfig.CommandTimeOut;//数据库超时时间，单位秒

             //SQL执行前回调函数
             db.Aop.OnLogExecuting = (sql, pars) =>
             {
                 //执行前可以输出SQL
                 //Console.WriteLine(sql);
             };

             //修改SQL和参数的值
             db.Aop.OnExecutingChangeSql = (sql, pars) =>
             {
                 return new KeyValuePair<string, SugarParameter[]>(sql, pars);
             };

             //SQL执行完回调函数
             db.Aop.OnLogExecuted = (sql, pars) =>
             {
                 //执行完可以输出SQL执行时间
                 Console.Write($"SQL:{sql},\r\nTimeSpan:{db.Ado.SqlExecutionTime.TotalMilliseconds}ms\r\n");
             };
         });
    }
}
