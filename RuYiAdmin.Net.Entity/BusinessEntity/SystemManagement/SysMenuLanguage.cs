﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 多语菜单关系模型
    /// </summary>
    [SugarTable("sys_menu_language")]
    public class SysMenuLanguage : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 菜单编号
        /// </summary>
        [Required]
        public Guid MenuId { get; set; }

        /// <summary>
        /// 语言编号
        /// </summary>
        [Required]
        public Guid LanguageId { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [Required, MaxLength(128)]
        public String MenuName { get; set; }
    }
}
