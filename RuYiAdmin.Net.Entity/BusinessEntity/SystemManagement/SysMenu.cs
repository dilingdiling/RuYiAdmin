﻿using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEnum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 菜单模型
    /// </summary>
    [SugarTable("sys_menu")]
    public class SysMenu : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 父路径
        /// </summary>
        [MaxLength(256)]
        public String Path { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [Required, MaxLength(128)]
        public String MenuName { get; set; }

        /// <summary>
        /// 菜单路径
        /// </summary>
        [MaxLength(256)]
        public String MenuUrl { get; set; }

        /// <summary>
        /// 父键
        /// </summary>
        public Nullable<Guid> ParentId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Required]
        public MenuType MenuType { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        [MaxLength(256)]
        public String Icon { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [MaxLength(125)]
        public String Code { get; set; }
    }
}
