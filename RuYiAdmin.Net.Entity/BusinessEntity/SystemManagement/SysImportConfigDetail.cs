﻿using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.Entity.CoreEnum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 导入详细配置模型
    /// </summary>
    [SugarTable("sys_import_config_detail")]
    public class SysImportConfigDetail : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 父键
        /// </summary>
        [Required]
        public Guid ParentId { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        [Required]
        public CellDataType DataType { get; set; }

        /// <summary>
        /// 所在列
        /// </summary>
        [Required, MaxLength(512)]
        public String Cells { get; set; }

        /// <summary>
        /// 是否必填项
        /// 0：否
        /// 1：是
        /// </summary>
        public Nullable<int> Required { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public Nullable<Double> MaxValue { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public Nullable<Double> MinValue { get; set; }

        /// <summary>
        /// 小数位上限
        /// </summary>
        public Nullable<int> DecimalLimit { get; set; }

        /// <summary>
        /// 枚举列表
        /// </summary>
        public String TextEnum { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public String Extend1 { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public String Extend2 { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public String Extend3 { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }
    }
}
