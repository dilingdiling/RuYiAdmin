﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 数据字典模型
    /// </summary>
    [SugarTable("sys_code_table")]
    public class SysCodeTable : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 编码名称
        /// </summary>
        [Required, MaxLength(128)]
        public String CodeName { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Required, MaxLength(128)]
        public String Code { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [MaxLength(256)]
        public String Value { get; set; }

        /// <summary>
        /// 父键
        /// </summary>
        public Nullable<Guid> ParentId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }
    }
}
