﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 系统记录表
    /// </summary>
    [SugarTable("sys_business_record")]

    public class SysBusinessRecord : RuYiAdminBaseEntity
    {
    }
}
