﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 导入主配置模型
    /// </summary>
    [SugarTable("sys_import_config")]
    public class SysImportConfig : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 配置名称
        /// </summary>
        [Required, MaxLength(128)]
        public String ConfigName { get; set; }

        /// <summary>
        /// 起始行
        /// </summary>
        [Required]
        public int StartRow { get; set; }

        /// <summary>
        /// 起始列
        /// </summary>
        [Required]
        public int StartColumn { get; set; }

        /// <summary>
        /// 工作簿索引列表
        /// </summary>
        public String WorkSheetIndexes { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }
    }
}
