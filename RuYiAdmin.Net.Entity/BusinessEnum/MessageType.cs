﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEnum
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 强制下线
        /// </summary>
        ForceLogout,

        /// <summary>
        /// 通知
        /// </summary>
        Notice,

        /// <summary>
        /// 公告
        /// </summary>
        Announcement,

        /// <summary>
        /// 广播
        /// </summary>
        Broadcast
    }
}
