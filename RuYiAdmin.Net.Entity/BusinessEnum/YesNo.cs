﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEnum
{
    /// <summary>
    /// YesNo枚举
    /// </summary>
    public enum YesNo
    {
        /// <summary>
        /// NO
        /// </summary>
        NO,

        /// <summary>
        /// YES
        /// </summary>
        YES
    }
}
