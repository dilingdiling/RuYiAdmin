﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 用户DTO
    /// </summary>
    public class SysUserDTO : SysUser
    {
        /// <summary>
        /// 机构编号
        /// </summary>
        [Required]
        public Guid OrgId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public String OrgName { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public String Token { get; set; }

        /// <summary>
        /// token有效时间
        /// 单位：秒
        /// </summary>
        public int TokenExpiration { get; set; }
    }
}
