﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 菜单DTO
    /// </summary>
    public class SysMenuDTO : SysMenu
    {
        /// <summary>
        /// 子集
        /// </summary>
        public List<SysMenuDTO> Children { get; set; }

        /// <summary>
        /// 英文菜单名称
        /// </summary>
        public String MenuNameEn { get; set; }

        /// <summary>
        /// 俄文菜单名称
        /// </summary>
        public String MenuNameRu { get; set; }
    }
}
