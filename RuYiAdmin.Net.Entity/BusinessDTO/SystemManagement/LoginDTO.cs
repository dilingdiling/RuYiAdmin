﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 用户登录DTO
    /// </summary>
    public class LoginDTO
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        public String UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public String Password { get; set; }

        /// <summary>
        /// 验证码编号
        /// </summary>
        public String CaptchaId { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public String Captcha { get; set; }
    }
}
