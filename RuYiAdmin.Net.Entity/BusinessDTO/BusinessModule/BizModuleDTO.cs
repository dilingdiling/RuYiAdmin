﻿using RuYiAdmin.Net.Entity.BusinessEntity.BusinessModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.BusinessModule
{
    /// <summary>
    /// BizModule DTO
    /// </summary>
    public class BizModuleDTO: BizModule
    {
        /// <summary>
        /// 用户所在模块登录账号
        /// </summary>
        public String UserModuleLogonName { get; set; }

        /// <summary>
        /// 用户所在模块登录密码
        /// </summary>
        public String UserModulePassword { get; set; }
    }
}
