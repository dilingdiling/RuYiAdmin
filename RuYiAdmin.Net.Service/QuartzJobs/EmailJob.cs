﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.QuartzJobs
{
    public class EmailJob : IJob
    {
        public EmailJob()
        {
        }

        public Task Execute(IJobExecutionContext context)
        {
            //执行定时发送邮件作业
            return Task.CompletedTask;
        }
    }
}