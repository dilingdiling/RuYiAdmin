//-----------------------------------------------------------------------
// <Copyright file="IBizUserService.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: IBizUserService.cs
// * History : Created by RuYiAdmin 01/21/2022 13:40:04
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.Entity.BusinessDTO.BusinessModule;
using RuYiAdmin.Net.Entity.BusinessEntity.BusinessModule;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.BusinessModule.BusinessUserService
{
    /// <summary>
    /// BizUser业务层接口
    /// </summary>   
    public interface IBizUserService : IRuYiAdminBaseService<BizUser>
    {
        /// <summary>
        /// 获取业务用户
        /// </summary>
        /// <param name="moduleShortName">模块短名</param>
        /// <param name="logonName">登陆账号</param>
        /// <returns>业务用户</returns>
        Task<BizUser> GetBizUser(String moduleShortName, String logonName);
    }
}
