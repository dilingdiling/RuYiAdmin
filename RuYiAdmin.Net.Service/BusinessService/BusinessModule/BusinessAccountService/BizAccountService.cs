//-----------------------------------------------------------------------
// <Copyright file="BizAccountService.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: BizAccountService.cs
// * History : Created by RuYiAdmin 03/14/2022 09:21:40
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.Entity.BusinessEntity.BusinessModule;
using RuYiAdmin.Net.Repository.BusinessRepository.BusinessModule.BusinessAccountRepository;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.BusinessModule.BusinessAccountService
{
    /// <summary>
    /// BizAccount业务层实现
    /// </summary>
    public class BizAccountService : RuYiAdminBaseService<BizAccount>, IBizAccountService
    {
        #region 属性及构造函数

        /// <summary>
        /// 仓储实例
        /// </summary>
        private readonly IBizAccountRepository BizAccountRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="BizAccountRepository"></param>
        public BizAccountService(IBizAccountRepository BizAccountRepository) : base(BizAccountRepository)
        {
            this.BizAccountRepository = BizAccountRepository;
        }

        #endregion
    }
}
