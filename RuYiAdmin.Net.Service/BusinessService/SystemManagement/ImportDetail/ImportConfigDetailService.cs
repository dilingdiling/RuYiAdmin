﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.ImportDetail;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.ImportDetail
{
    /// <summary>
    /// 导入配置明细业务层实现
    /// </summary>
    public class ImportConfigDetailService : RuYiAdminBaseService<SysImportConfigDetail>, IImportConfigDetailService
    {
        /// <summary>
        /// 导入配置明细访问层实例
        /// </summary>
        private readonly IImportConfigDetailRepository importConfigDetailRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="importConfigDetailRepository"></param>
        public ImportConfigDetailService(IImportConfigDetailRepository importConfigDetailRepository) : base(importConfigDetailRepository)
        {
            this.importConfigDetailRepository = importConfigDetailRepository;
        }
    }
}
