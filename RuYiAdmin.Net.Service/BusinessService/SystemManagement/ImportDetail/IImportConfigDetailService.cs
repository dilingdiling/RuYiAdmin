﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.ImportDetail
{
    /// <summary>
    /// 导入配置明细业务层接口
    /// </summary>
    public interface IImportConfigDetailService : IRuYiAdminBaseService<SysImportConfigDetail>
    {
    }
}
