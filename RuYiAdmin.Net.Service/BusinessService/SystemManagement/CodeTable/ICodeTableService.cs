﻿using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.CodeTable
{
    /// <summary>
    /// 数据字典业务层接口
    /// </summary>
    public interface ICodeTableService : IRuYiAdminBaseService<SysCodeTable>
    {
        /// <summary>
        /// 获取字典树
        /// </summary>
        /// <returns>ActionResult</returns>
        Task<QueryResult<SysCodeTableDTO>> GetCodeTreeNodes();

        /// <summary>
        /// 加载数据字典缓存
        /// </summary>
        Task LoadSystemCodeTableCache();

        /// <summary>
        /// 清理数据字典缓存
        /// </summary>
        Task ClearSystemCodeTableCache();
    }
}
