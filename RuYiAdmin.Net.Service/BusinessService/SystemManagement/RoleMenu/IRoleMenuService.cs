﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleMenu
{
    /// <summary>
    /// 角色菜单业务层接口
    /// </summary>
    public interface IRoleMenuService : IRuYiAdminBaseService<SysRoleMenu>
    {
        /// <summary>
        /// 加载角色与菜单缓存
        /// </summary>
        Task LoadSystemRoleMenuCache();

        /// <summary>
        /// 清理角色与菜单缓存
        /// </summary>
        Task ClearSystemRoleMenuCache();
    }
}
