﻿using MongoDB.Driver;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Log
{
    /// <summary>
    /// 审计日志工具类
    /// </summary>
    public static class SysLogExtension
    {
        /// <summary>
        /// 记录审计日志
        /// </summary>
        /// <param name="log">日志对象</param>
        /// <returns></returns>
        public static async Task WriteAsync(this SysLog log)
        {
            //判断审计日志记录开关是否开启
            if (!GlobalContext.LogConfig.IsEnabled)
            {
                return;
            }

            //支持MongoDB
            if (GlobalContext.LogConfig.SupportMongoDB)
            {
                //审计日志存入非关系库
                IMongoDatabase db = MongoUtil.Instance.GetDatabase(GlobalContext.LogConfig.MongoDefaultDB);
                IMongoCollection<SysLog> mongoCollection = db.GetCollection<SysLog>("SysLog");
                await mongoCollection.InsertOneAsync(log);
            }
            else
            {
                //审计日志存入关系库
                await RuYiAdminDbContext.RuYiDbContext.Insertable<SysLog>(log).ExecuteCommandAsync();
            }
        }
    }
}
