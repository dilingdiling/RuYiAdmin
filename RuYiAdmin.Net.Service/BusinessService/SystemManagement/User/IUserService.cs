﻿using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.User
{
    /// <summary>
    /// 用户业务层接口
    /// </summary>
    public interface IUserService : IRuYiAdminBaseService<SysUser>
    {
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <returns>ActionResult</returns>
        Task<ActionResult> DeleteEntity(Guid userId);

        /// <summary>
        /// 批量删除用户
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>ActionResult</returns>
        Task<ActionResult> DeleteEntities(String ids);

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns>ActionResult</returns>
        ActionResult GetCaptcha();

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginDTO">登录信息</param>
        /// <returns>ActionResult</returns>
        Task<ActionResult> Logon(LoginDTO loginDTO);

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>ActionResult</returns>
        Task<ActionResult> Logout(String token);

        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="data">参数</param>
        /// <returns>ActionResult</returns>
        Task<ActionResult> UpdatePassword(PasswordDTO data);

        /// <summary>
        /// 获取在线用户
        /// </summary>
        /// <returns>QueryResult</returns>
        Task<QueryResult<SysUserDTO>> GetOnlineUsers();

        /// <summary>
        /// 加载系统用户缓存
        /// </summary>
        Task LoadSystemUserCache();

        /// <summary>
        /// 清理系统用户缓存
        /// </summary>
        Task ClearSystemUserCache();
    }
}
