﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Role
{
    /// <summary>
    /// 角色业务层接口
    /// </summary>
    public interface IRoleService : IRuYiAdminBaseService<SysRole>
    {
        /// <summary>
        /// 加载系统角色缓存
        /// </summary>
        Task LoadSystemRoleCache();

        /// <summary>
        /// 清理系统角色缓存
        /// </summary>
        Task ClearSystemRoleCache();
    }
}
