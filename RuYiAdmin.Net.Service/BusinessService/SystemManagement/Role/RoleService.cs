﻿using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Repository.BusinessRepository.Redis;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.Role;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Role
{
    /// <summary>
    /// 角色业务层实现
    /// </summary>
    class RoleService : RuYiAdminBaseService<SysRole>, IRoleService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色仓储实例
        /// </summary>
        private readonly IRoleRepository roleRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository redisRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="roleRepository"></param>
        /// <param name="redisRepository"></param>
        public RoleService(IRoleRepository roleRepository,
                           IRedisRepository redisRepository) : base(roleRepository)
        {
            this.roleRepository = roleRepository;
            this.redisRepository = redisRepository;
        }

        #endregion

        #region 公有方法

        #region 加载系统角色缓存

        /// <summary>
        /// 加载系统角色缓存
        /// </summary>
        public async Task LoadSystemRoleCache()
        {
            var sqlKey = "sqls:sql:query_role_info";
            var strSQL = GlobalContext.Configuration.GetSection(sqlKey).Value;

            int totalCount = 0;
            var roles = await this.roleRepository.SqlQueryAsync<SysRoleDTO>(new QueryCondition(), totalCount, strSQL);

            await this.redisRepository.SetAsync(GlobalContext.SystemCacheConfig.RoleCacheName, roles, -1);
        }

        #endregion

        #region 清理系统角色缓存

        /// <summary>
        /// 清理系统角色缓存
        /// </summary>
        public async Task ClearSystemRoleCache()
        {
            await this.redisRepository.DeleteAsync(new String[] { GlobalContext.SystemCacheConfig.RoleCacheName });
        }

        #endregion

        #endregion
    }
}
