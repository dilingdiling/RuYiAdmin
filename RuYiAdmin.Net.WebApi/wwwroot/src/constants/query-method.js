export const QueryMethod = {
  // / <summary>
  // / 等于
  // / </summary>
  Equal: 0,

  // / <summary>
  // / 模糊查询
  // / </summary>
  Like: 1,

  // / <summary>
  // / 小于
  // / </summary>
  LessThan: 2,

  // / <summary>
  // / 小于等于
  // / </summary>
  LessThanOrEqual: 3,

  // / <summary>
  // / 大于
  // / </summary>
  GreaterThan: 4,

  // / <summary>
  // / 大于等于
  // / </summary>
  GreaterThanOrEqual: 5,

  // / <summary>
  // / BetweenAnd
  // / </summary>
  BetweenAnd: 6,

  // / <summary>
  // / Include
  // / </summary>
  Include: 7,

  // / <summary>
  // / 模糊查询
  // / </summary>
  OrLike: 8,

  // / <summary>
  // / 不等于
  // / </summary>
  NotEqual: 9
}
