export const BusinessLanguage = {
  Common: {
    Button: {
      Add: {
        CN: '新增',
        EN: 'Add',
        RU: 'Add'
      },
      Edit: {
        CN: '编辑',
        EN: 'Edit',
        RU: 'Edit'
      },
      Delete: {
        CN: '删除',
        EN: 'Delete',
        RU: 'Delete',
        Confirm: {
          CN: '确定要将数据删除吗?',
          EN: 'are you sure you want to delete the data?',
          RU: 'are you sure you want to delete the data?'
        }
      },
      Refresh: {
        CN: '刷新',
        EN: 'Refresh',
        RU: 'Refresh'
      },
      Search: {
        CN: '查询',
        EN: 'Search',
        RU: 'Search'
      },
      Reset: {
        CN: '重置',
        EN: 'Reset',
        RU: 'Reset'
      }
    },
    Dialog: {
      Sure: {
        CN: '确 定',
        EN: 'sure',
        RU: 'sure'
      },
      Cancel: {
        CN: '取 消',
        EN: 'cancel',
        RU: 'cancel'
      },
      Tooltip: {
        CN: '提示',
        EN: 'tooltip',
        RU: 'tooltip'
      },
      Message: {
        Save: {
          CN: '保存成功',
          EN: 'success',
          RU: 'success'
        },
        Edit: {
          CN: '编辑成功',
          EN: 'success',
          RU: 'success'
        },
        Delete: {
          CN: '删除成功',
          EN: 'success',
          RU: 'success'
        }
      }
    },
    Grid: {
      Index: {
        CN: '序号',
        EN: 'index',
        RU: 'index'
      },
      SerialNumber: {
        CN: '排序',
        EN: 'serial number',
        RU: 'serial number'
      },
      Remark: {
        CN: '备注',
        EN: 'remark',
        RU: 'remark'
      },
      IsDel: {
        CN: '是否删除',
        EN: 'is deleted',
        RU: 'is deleted'
      },
      Creator: {
        CN: '创建人',
        EN: 'creator',
        RU: 'creator'
      },
      CreateTime: {
        CN: '创建时间',
        EN: 'create time',
        RU: 'create time'
      },
      Modifier: {
        CN: '修改人',
        EN: 'modifier',
        RU: 'modifier'
      },
      ModifyTime: {
        CN: '修改时间',
        EN: 'modify time',
        RU: 'modify time'
      },
      Action: {
        CN: '操作',
        EN: 'action',
        RU: 'action'
      }
    },
    Form: {
      Placeholder: {
        SerialNumber: {
          CN: '请输入序号',
          EN: 'please input serial number',
          RU: 'please input serial number'
        },
        Remark: {
          CN: '请输入备注',
          EN: 'please input note',
          RU: 'please input note'
        }
      }
    }
  },
  Org: {
    Grid: {
      OrgName: {
        CN: '机构名称',
        EN: 'org name',
        RU: 'org name'
      },
      LeaderName: {
        CN: '主管人姓名',
        EN: 'leader name',
        RU: 'leader name'
      }
    },
    DialogTitle: {
      Add: {
        CN: '新增机构信息',
        EN: 'add org info',
        RU: 'add org info'
      },
      Edit: {
        CN: '编辑机构信息',
        EN: 'edit org info',
        RU: 'edit org info'
      },
      DefaultSelection: {
        CN: '请选择父机构',
        EN: 'please select parent org',
        RU: 'please select parent org'
      }
    },
    Form: {
      Placeholder: {
        OrgName: {
          CN: '请输入机构名称',
          EN: 'please input org name',
          RU: 'please input org name'
        },
        LeaderName: {
          CN: '请选择主管人',
          EN: 'please select leader name',
          RU: 'please select leader name'
        }
      },
      Rule: {
        OrgName: {
          MinLengthMessage: {
            CN: '长度在 0 到 128 个字符',
            EN: 'length less than or equal 128',
            RU: 'length less than or equal 128'
          }
        }
      }
    }
  },
  Language: {
    Grid: {
      LanguageName: {
        CN: '语言名称',
        EN: 'language name',
        RU: 'language name'
      }
    }
  },
  Menu: {
    DialogTitle: {
      Add: {
        CN: '新增菜单信息',
        EN: 'add menu info',
        RU: 'add menu info'
      },
      Edit: {
        CN: '编辑菜单信息',
        EN: 'edit menu info',
        RU: 'edit menu info'
      }
    },
    Form: {
      Field: {
        RootPath: {
          CN: '根目录',
          EN: 'root path',
          RU: 'root path'
        },
        MenuName: {
          CN: '菜单名称',
          EN: 'menu name',
          RU: 'menu name'
        },
        MenuNameEn: {
          CN: '英文菜单',
          EN: 'menu name in English',
          RU: 'menu name in English'
        },
        MenuNameRu: {
          CN: '俄文菜单',
          EN: 'menu name in Russian',
          RU: 'menu name in Russian'
        },
        MenuUrl: {
          CN: '菜单路径',
          EN: 'menu url',
          RU: 'menu url'
        },
        MenuType: {
          CN: '菜单类型',
          EN: 'menu type',
          RU: 'menu type'
        },
        Icon: {
          CN: '菜单图标',
          EN: 'menu icon',
          RU: 'menu icon'
        },
        Code: {
          CN: '编码',
          EN: 'code',
          RU: 'code'
        }
      },
      Placeholder: {
        RootPath: {
          CN: '请输入根目录',
          EN: 'please input root path',
          RU: 'please input root path'
        },
        MenuName: {
          CN: '请输入中文菜单名称',
          EN: 'please input menu name',
          RU: 'please input menu name'
        },
        MenuNameEn: {
          CN: '请输入英文菜单名称',
          EN: 'please input menu name in English',
          RU: 'please input menu name in English'
        },
        MenuNameRu: {
          CN: '请输入俄文菜单名称',
          EN: 'please input menu name in Russian',
          RU: 'please input menu name in Russian'
        },
        MenuUrl: {
          CN: '请输入菜单路径',
          EN: 'please input menu url',
          RU: 'please input menu url'
        },
        MenuType: {
          CN: '请选择菜单类型',
          EN: 'please select menu type',
          RU: 'please select menu type'
        },
        Icon: {
          CN: '请输入菜单图标',
          EN: 'please input menu icon',
          RU: 'please input menu icon'
        },
        Code: {
          CN: '请输入编码',
          EN: 'please input code',
          RU: 'please input code'
        }
      },
      Rule: {
        MenuName: {
          MinLengthMessage: {
            CN: '长度在 0 到 128 个字符',
            EN: 'length less than or equal 128',
            RU: 'length less than or equal 128'
          }
        }
      },
      Selection: {
        MenuType: {
          Menu: {
            CN: '菜单',
            EN: 'menu',
            RU: 'menu'
          },
          Button: {
            CN: '按钮',
            EN: 'button',
            RU: 'button'
          },
          View: {
            CN: '视图',
            EN: 'view',
            RU: 'view'
          }
        }
      }
    }
  },
  CodeTable: {
    Grid: {
      CodeName: {
        CN: '字典名称',
        EN: 'code name',
        RU: 'code name'
      },
      Code: {
        CN: '编码',
        EN: 'code',
        RU: 'code'
      },
      CodeValue: {
        CN: '编码值',
        EN: 'code value',
        RU: 'code value'
      }
    },
    DialogTitle: {
      Add: {
        CN: '新增字典信息',
        EN: 'add code info',
        RU: 'add code info'
      },
      Edit: {
        CN: '编辑字典信息',
        EN: 'edit code info',
        RU: 'edit code info'
      }
    },
    Form: {
      Placeholder: {
        CodeName: {
          CN: '请输入字典名称',
          EN: 'please input code name',
          RU: 'please input code name'
        },
        Code: {
          CN: '请输入编码',
          EN: 'please input code',
          RU: 'please input code'
        },
        CodeValue: {
          CN: '请输入编码值',
          EN: 'please input code value',
          RU: 'please input code value'
        }
      },
      Rule: {
        CodeName: {
          MinLengthMessage: {
            CN: '长度在 0 到 128 个字符',
            EN: 'length less than or equal 128',
            RU: 'length less than or equal 128'
          }
        },
        Code: {
          MinLengthMessage: {
            CN: '长度在 0 到 128 个字符',
            EN: 'length less than or equal 128',
            RU: 'length less than or equal 128'
          }
        }
      }
    }
  },
  OnlineUser: {
    Grid: {
      LogonName: {
        CN: '登录账号',
        EN: 'logon name',
        RU: 'logon name'
      },
      DisplayName: {
        CN: '用户姓名',
        EN: 'user name',
        RU: 'user name'
      },
      OrgName: {
        CN: '所在机构',
        EN: 'org name',
        RU: 'org name'
      },
      ForceLogout: {
        Action: {
          CN: '下线',
          EN: 'force logout',
          RU: 'force logout'
        },
        WarnningMessage: {
          CN: '不可下线超级用户',
          EN: 'can not force admin logout',
          RU: 'can not force admin logout'
        },
        SuccessMessage: {
          CN: '下线成功',
          EN: 'successfully force logout',
          RU: 'successfully force logout'
        },
        Tooltip: {
          CN: '确定要将用户下线吗?',
          EN: 'are you sure you force user logout?',
          RU: 'are you sure you force user logout?'
        }
      }
    }
  },
  ServerMonitor: {
    Title: {
      CN: '操作系统、CPU、内存',
      EN: 'OS、CPU、Memory',
      RU: 'OS、CPU、Memory'
    },
    HostName: {
      CN: '计算机名称',
      EN: 'host name',
      RU: 'host name'
    },
    Windows: {
      CN: 'Windows操作系统',
      EN: 'Windows',
      RU: 'Windows'
    },
    ComputerArchitecture: {
      CN: '计算机架构',
      EN: 'computer architecture',
      RU: 'computer architecture'
    },
    OSDescription: {
      CN: '操作系统名称',
      EN: 'OS description',
      RU: 'OS description'
    },
    OSArchitecture: {
      CN: '操作系统架构',
      EN: 'OS architecture',
      RU: 'OS architecture'
    },
    FrameworkDescription: {
      CN: '.NET平台',
      EN: 'framework description',
      RU: 'framework description'
    },
    CPUCount: {
      CN: 'CPU数量',
      EN: 'CPU count',
      RU: 'CPU count'
    },
    CPUCoreCount: {
      CN: 'CPU内核数量',
      EN: 'CPU core count',
      RU: 'CPU core count'
    },
    CPULoad: {
      CN: 'CPU负载',
      EN: 'CPU load',
      RU: 'CPU load'
    },
    CPUTemperature: {
      CN: 'CPU温度',
      EN: 'CPU temperature',
      RU: 'CPU temperature'
    },
    CPUProcessBit:
      {
        CN: 'CPU处理器位数',
        EN: 'CPU process bits',
        RU: 'CPU process bits'
      },
    CPUProcessArchitecture: {
      CN: 'CPU处理器架构',
      EN: 'CPU process architecture',
      RU: 'CPU process architecture'
    },
    MemorySize: {
      CN: '内存空间',
      EN: 'memory size',
      RU: 'memory size'
    },
    AvailableMemorySize: {
      CN: '可用内存',
      EN: 'available memory size',
      RU: 'available memory size'
    },
    Storage: {
      CN: '存储',
      EN: 'storage',
      RU: 'storage'
    },
    LogicalDrives: {
      CN: '逻辑驱动盘',
      EN: 'logical drives',
      RU: 'logical drives'
    },
    SerialNumber: {
      CN: '序列号',
      EN: 'serial number',
      RU: 'serial number'
    },
    Model: {
      CN: '型号',
      EN: 'model',
      RU: 'model'
    },
    StorageSize: {
      CN: '空间大小',
      EN: 'storage size',
      RU: 'storage size'
    }
  },
  ScheduleJob: {
    Grid: {
      Button: {
        Start: {
          CN: '启动',
          EN: 'Start',
          RU: 'Start'
        },
        Pause: {
          CN: '暂停',
          EN: 'Pause',
          RU: 'Pause'
        },
        Resume: {
          CN: '恢复',
          EN: 'Resume',
          RU: 'Resume'
        }
      }
    },
    Form: {
      DialogTitle: {

      }
    }
  },
  GetMenuName: function(type) {
    const language = localStorage.getItem('language')
    switch (language) {
      case 'zh-CN':
        return type.CN
      case 'en-US':
        return type.EN
      case 'ru-RU':
        return type.RU
      default:
        break
    }
  }
}
