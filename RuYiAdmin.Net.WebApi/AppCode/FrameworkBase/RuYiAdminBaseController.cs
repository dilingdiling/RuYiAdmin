﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using RuYiAdmin.Net.WebApi.AppCode.ActionFilters;
using RuYiAdmin.Net.WebApi.AppCode.AuthorizationFilter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.FrameworkBase
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(GlobalContext.RouteTemplate)]
    public class RuYiAdminBaseController<T> : ControllerBase where T : RuYiAdminBaseEntity
    {
        /*声明：

        1.该基类方法可用于构建独立的WebApi。

        2.方法不够用时可调用基类服务、自行添加。

        3.按钮与视图的权限鉴别，请在具体的业务控制器中实现。

        */

        #region 属性与构造函数

        /// <summary>
        /// 服务层基类实例
        /// </summary>
        private readonly IRuYiAdminBaseService<T> RuYiAdminBaseService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RuYiAdminBaseService"></param>
        public RuYiAdminBaseController(IRuYiAdminBaseService<T> RuYiAdminBaseService)
        {
            this.RuYiAdminBaseService = RuYiAdminBaseService;
        }

        #endregion        

        #region 通用方法

        /// <summary>
        /// 分页查询列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> GetPage(QueryCondition queryCondition)
        {
            var actionResult = await this.RuYiAdminBaseService.GetPageAsync(queryCondition);
            return Ok(actionResult);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> GetList(QueryCondition queryCondition)
        {
            var actionResult = await this.RuYiAdminBaseService.GetListAsync(queryCondition);
            return Ok(actionResult);
        }

        /// <summary>
        /// 按编号查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        public async Task<IActionResult> GetEntityById(Guid id)
        {
            var actionResult = await this.RuYiAdminBaseService.GetByIdAsync(id);
            return Ok(actionResult);
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        public async Task<IActionResult> AddEntity([FromBody] T t)
        {
            var actionResult = await this.RuYiAdminBaseService.AddAsync(t);
            return Ok(actionResult);
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>ActionResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        public async Task<IActionResult> EditEntity([FromBody] T t)
        {
            var actionResult = await this.RuYiAdminBaseService.UpdateAsync(t);
            return Ok(actionResult);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteEntityById(Guid id)
        {
            var actionResult = await this.RuYiAdminBaseService.DeleteAsync(id);
            return Ok(actionResult);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteEntitiesByIds(String ids)
        {
            var array = StringUtil.GetGuids(ids);
            var actionResult = await this.RuYiAdminBaseService.DeleteRangeAsync(array);
            return Ok(actionResult);
        }

        /// <summary>
        /// 下载Excel
        /// </summary>
        /// <param name="excelId">文件编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{excelId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadExcel(String excelId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(GlobalContext.DirectoryConfig.GetTempPath(), "/");
                //文件路径
                var filePath = Path.Join(path, excelId + ".xls");
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", "错误信息.xls");
            });
        }

        /// <summary>
        /// 下载Excel模板
        /// </summary>
        /// <param name="templateId">文件编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{templateId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadTemplate(String templateId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(GlobalContext.DirectoryConfig.GetTemplateDirectory(), "/");
                //文件路径
                var filePath = Path.Join(path, templateId + ".xls");
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", "导入模板.xls");
            });
        }

        #endregion
    }
}
