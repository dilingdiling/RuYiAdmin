﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RuYiAdmin.Net.Common.CommonClass.Exceptions;
using RuYiAdmin.Net.WebApi.AppCode.FrameworkClass;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.FrameworkExtensions
{
    /// <summary>
    /// 全局异常处理中间件
    /// </summary>
    public class GlobalExceptionHandlerExtensions
    {
        private readonly RequestDelegate _next;  //上下文请求 
        private readonly ILogger<GlobalExceptionHandlerExtensions> _logger;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="next">上下文请求</param>
        /// <param name="logger">日志对象</param>
        public GlobalExceptionHandlerExtensions(RequestDelegate next, ILogger<GlobalExceptionHandlerExtensions> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// 处理上下文请求
        /// </summary>
        /// <param name="httpContext">http会话对象</param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext); //处理上下文请求
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex); //捕获异常,在HandleExceptionAsync中处理
            }
        }

        /// <summary>
        /// 全局异常处理
        /// </summary>
        /// <param name="context">http会话对象</param>
        /// <param name="exception">全局异常处理</param>
        /// <returns></returns>
        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";  //返回json 类型
            var response = context.Response;

            var errorResponse = new ErrorResponse { Success = false };

            // 自定义的异常错误信息类型
            switch (exception)
            {
                case ApplicationException ex:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    errorResponse.Message = ex.Message;
                    break;
                case RuYiAdminCustomException ex:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    errorResponse.Message = ex.Message;
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    errorResponse.Message = "Internal Server Error";
                    break;
            }

            //控制台输出异常信息
            _logger.LogError(exception.Message);

            //返回统一异常
            var result = errorResponse;
            await context.Response.WriteAsync(result.ToJson());
        }
    }
}
