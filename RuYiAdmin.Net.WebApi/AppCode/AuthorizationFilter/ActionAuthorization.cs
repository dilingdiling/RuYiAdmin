﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.AuthorizationFilter
{
    public class ActionAuthorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //处理匿名方法
            foreach (var item in context.ActionDescriptor.EndpointMetadata)
            {
                if (item.GetType().Name.Equals(typeof(AllowAnonymousAttribute).Name))
                {
                    return;
                }
            }

            //处理白名单
            var whiteList = GlobalContext.SystemConfig.WhiteList;
            if (!String.IsNullOrEmpty(whiteList))
            {
                var array = whiteList.Split(',');
                var url = context.HttpContext.Request.Path.Value;
                foreach (var item in array)
                {
                    if (url.StartsWith(item))
                    {
                        return;
                    }
                }
            }

            //Jwt验证
            if (GlobalContext.SystemConfig.CheckJwtToken)
            {
                if (!context.HttpContext.Request.Headers.ContainsKey("Authorization"))
                {
                    context.Result = new UnauthorizedObjectResult("unauthorized jwt token");
                    return;
                }
            }

            //Token验证
            if (GlobalContext.SystemConfig.CheckToken)
            {
                if (!context.HttpContext.Request.Headers.ContainsKey("token"))
                {
                    context.Result = new UnauthorizedObjectResult("token is neccesarry");
                    return;
                }
                else
                {
                    #region 续时操作

                    var token = context.HttpContext.GetToken();

                    //获取用户
                    var user = RedisUtil.Get<SysUserDTO>(token);
                    if (user != null)
                    {
                        var tokenExpiration = GlobalContext.JwtSettings.TokenExpiration * 60;

                        //续时
                        RedisUtil.Expire(token, tokenExpiration);
                    }
                    else
                    {
                        context.Result = new UnauthorizedObjectResult("invalid user token");
                        return;
                    }

                    #endregion
                }
            }

            //其他头部验证
            var headerConfig = GlobalContext.SystemConfig.HeaderConfig;
            if (!String.IsNullOrEmpty(headerConfig))
            {
                var array = headerConfig.Split(',');
                foreach (var item in array)
                {
                    if (!context.HttpContext.Request.Headers.ContainsKey(item))
                    {
                        context.Result = new BadRequestObjectResult(item.ToLower() + " is necessary");
                        return;
                    }
                }
            }

            base.OnActionExecuting(context);
        }
    }
}
