﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.IoCDependencyInjection
{
    /// <summary>
    /// 依赖自动注入
    /// </summary>
    public class DependencyAutoInjection : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //业务逻辑层程序集
            Assembly service = Assembly.Load("RuYiAdmin.Net.Service");
            //数据库访问层程序集
            Assembly repository = Assembly.Load("RuYiAdmin.Net.Repository");

            //自动注入
            builder.RegisterAssemblyTypes(service).Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces();
            //自动注入
            builder.RegisterAssemblyTypes(repository).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces();
        }
    }
}
