﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using RuYiAdmin.Net.Common.CommonClass.Extensions;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Log;
using RuYiAdmin.Net.WebApi.AppCode.ActionFilters;
using RuYiAdmin.Net.WebApi.AppCode.FrameworkBase;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.Controllers.SystemManagement
{
    /// <summary>
    /// 审计日志管理控制器
    /// </summary>
    public class LogManagementController : RuYiAdminBaseController<SysLog>
    {
        #region 属性及构造函数

        /// <summary>
        /// 审计日志接口实例
        /// </summary>
        private readonly ILogService logService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logService"></param>
        public LogManagementController(ILogService logService) : base(logService)
        {
            this.logService = logService;
        }

        #endregion

        #region 查询日志列表

        /// <summary>
        /// 查询日志列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("log:query:list")]
        public async Task<IActionResult> Post([FromBody] QueryCondition queryCondition)
        {
            //支持MongoDB
            if (GlobalContext.LogConfig.SupportMongoDB)
            {
                //从非关系库获取审计日志
                IMongoDatabase db = MongoUtil.Instance.GetDatabase(GlobalContext.LogConfig.MongoDefaultDB);
                IMongoCollection<SysLog> mongoCollection = db.GetCollection<SysLog>("SysLog");

                var logs = mongoCollection.AsQueryable().Where(QueryCondition.BuildExpression<SysLog>(queryCondition.QueryItems)).ToList();
                if (!String.IsNullOrEmpty(queryCondition.Sort))
                {
                    logs = logs.Sort<SysLog>(queryCondition.Sort);
                }

                var queryList = logs.Skip(queryCondition.PageIndex * queryCondition.PageSize).Take(queryCondition.PageSize).ToList();

                var actionResult = QueryResult<SysLog>.Success(logs.Count, queryList);
                return Ok(actionResult);
            }
            else
            {
                var key = "sqls:sql:query_syslog_ms";

                if ((DbType)GlobalContext.DBConfig.DBType == DbType.MySql)
                {
                    key = "sqls:sql:query_syslog";
                }

                var actionResult = await this.logService.SqlQueryAsync(queryCondition, key);
                return Ok(actionResult);
            }
        }

        #endregion

        #region 获取日志明细

        /// <summary>
        /// 获取日志明细
        /// </summary>
        /// <param name="logId">日志编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{logId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("log:query:entity")]
        public async Task<IActionResult> GetById(Guid logId)
        {
            var actionResult = await this.logService.GetByIdAsync(logId);
            return Ok(actionResult);
        }

        #endregion

        #region 下载返回数据

        /// <summary>
        /// 下载返回数据
        /// </summary>
        /// <param name="txtId">日志编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{txtId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadMonitoringLog(String txtId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(GlobalContext.DirectoryConfig.GetMonitoringLogsPath(), "/");
                //文件名称
                var fileName = txtId + ".txt";
                //文件路径
                var filePath = Path.Join(path, fileName);
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", fileName);
            });
        }

        #endregion
    }
}
