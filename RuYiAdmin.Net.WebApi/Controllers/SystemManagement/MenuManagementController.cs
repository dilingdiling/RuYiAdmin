﻿using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.Common.CommonClass.Extensions;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.BusinessService.Redis;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Menu;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.MenuLanguage;
using RuYiAdmin.Net.WebApi.AppCode.ActionFilters;
using RuYiAdmin.Net.WebApi.AppCode.FrameworkBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.Controllers.SystemManagement
{
    /// <summary>
    /// 菜单管理控制器
    /// </summary>
    public class MenuManagementController : RuYiAdminBaseController<SysMenu>
    {
        #region 属性及构造函数

        /// <summary>
        /// 菜单服务实例
        /// </summary>
        private readonly IMenuService menuService;

        /// <summary>
        /// 菜单多语实例
        /// </summary>
        private readonly IMenuLanguageService menuLanguageService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService redisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="menuService"></param>
        /// <param name="menuLanguageService"></param>
        /// <param name="redisService"></param>
        public MenuManagementController(IMenuService menuService,
                                        IMenuLanguageService menuLanguageService,
                                        IRedisService redisService) : base(menuService)
        {
            this.menuService = menuService;
            this.menuLanguageService = menuLanguageService;
            this.redisService = redisService;
        }

        #endregion

        #region 查询菜单列表

        /// <summary>
        /// 查询菜单列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("menu:query:list")]
        public async Task<IActionResult> Post(QueryCondition queryCondition)
        {
            var actionResult = await this.menuService.GetMenuTreeNodes();
            return Ok(actionResult);
        }

        #endregion

        #region 获取菜单信息

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        [Permission("menu:query:list")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var actionResult = new Entity.CoreEntity.ActionResult();

            actionResult.HttpStatusCode = HttpStatusCode.OK;
            actionResult.Message = new String("OK");
            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);
            actionResult.Object = menus.Where(t => t.Id == id).FirstOrDefault();

            return Ok(actionResult);
        }

        #endregion

        #region 新增菜单信息

        /// <summary>
        /// 新增菜单信息
        /// </summary>
        /// <param name="sysMenu">菜单对象</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("menu:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysMenuDTO sysMenu)
        {
            var actionResult = await this.menuService.AddAsync(sysMenu);

            #region 维护菜单多语

            var languages = await this.redisService.GetAsync<List<SysLanguage>>(GlobalContext.SystemCacheConfig.LanguageCacheName);
            var menuLanguages = await this.redisService.GetAsync<List<SysMenuLanguage>>(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName);

            if (!String.IsNullOrEmpty(sysMenu.MenuNameEn))
            {
                var language = languages.Where(t => t.LanguageName.Equals("en-US")).FirstOrDefault();

                var menuLan = new SysMenuLanguage();
                menuLan.LanguageId = language.Id;
                menuLan.MenuId = sysMenu.Id;
                menuLan.MenuName = sysMenu.MenuNameEn;
                await this.menuLanguageService.AddAsync(menuLan);

                menuLanguages.Add(menuLan);
            }

            if (!String.IsNullOrEmpty(sysMenu.MenuNameRu))
            {
                var language = languages.Where(t => t.LanguageName.Equals("ru-RU")).FirstOrDefault();

                var menuLan = new SysMenuLanguage();
                menuLan.LanguageId = language.Id;
                menuLan.MenuId = sysMenu.Id;
                menuLan.MenuName = sysMenu.MenuNameRu;
                await this.menuLanguageService.AddAsync(menuLan);

                menuLanguages.Add(menuLan);
            }

            #endregion

            #region 数据一致性维护

            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);
            var menu = sysMenu;
            if (menu.Children != null)
            {
                menu.Children.Clear();
            }
            menus.Add(menu);
            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResult);
        }

        #endregion

        #region 修改菜单信息

        /// <summary>
        /// 修改菜单信息
        /// </summary>
        /// <param name="sysMenu">菜单对象</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("menu:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysMenuDTO sysMenu)
        {
            var actionResult = await this.menuService.UpdateAsync(sysMenu);

            #region 维护菜单多语

            var languages = await this.redisService.GetAsync<List<SysLanguage>>(GlobalContext.SystemCacheConfig.LanguageCacheName);
            var menuLanguages = await this.redisService.GetAsync<List<SysMenuLanguage>>(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName);

            if (!String.IsNullOrEmpty(sysMenu.MenuNameEn))
            {
                var language = languages.Where(t => t.LanguageName.Equals("en-US")).FirstOrDefault();

                var oldMenuLan = menuLanguages.Where(t => t.LanguageId.Equals(language.Id) && t.MenuId.Equals(sysMenu.Id) && t.IsDel.Equals(0)).
                                               FirstOrDefault();

                if (oldMenuLan != null && oldMenuLan.MenuName != sysMenu.MenuNameEn)
                {
                    await this.menuLanguageService.DeleteAsync(oldMenuLan.Id);

                    menuLanguages.Remove(oldMenuLan);

                    var menuLan = new SysMenuLanguage();
                    menuLan.LanguageId = language.Id;
                    menuLan.MenuId = sysMenu.Id;
                    menuLan.MenuName = sysMenu.MenuNameEn;
                    await this.menuLanguageService.AddAsync(menuLan);

                    menuLanguages.Add(menuLan);
                }
            }
            if (!String.IsNullOrEmpty(sysMenu.MenuNameRu))
            {
                var language = languages.Where(t => t.LanguageName.Equals("ru-RU")).FirstOrDefault();

                var oldMenuLan = menuLanguages.Where(t => t.LanguageId.Equals(language.Id) && t.MenuId.Equals(sysMenu.Id) && t.IsDel.Equals(0)).
                                               FirstOrDefault();

                if (oldMenuLan != null && oldMenuLan.MenuName != sysMenu.MenuNameRu)
                {
                    await this.menuLanguageService.DeleteAsync(oldMenuLan.Id);

                    menuLanguages.Remove(oldMenuLan);

                    var menuLan = new SysMenuLanguage();
                    menuLan.LanguageId = language.Id;
                    menuLan.MenuId = sysMenu.Id;
                    menuLan.MenuName = sysMenu.MenuNameRu;
                    await this.menuLanguageService.AddAsync(menuLan);

                    menuLanguages.Add(menuLan);
                }
            }

            #endregion

            #region 数据一致性维护

            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);
            var menu = menus.Where(t => t.Id == sysMenu.Id).FirstOrDefault();
            menus.Remove(menu);
            menu = sysMenu;
            if (menu.Children != null)
            {
                menu.Children.Clear();
            }
            menus.Add(menu);
            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResult);
        }

        #endregion

        #region 删除菜单信息

        /// <summary>
        /// 删除菜单信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("menu:del:entities")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (await this.Check(id))
            {
                return BadRequest("menu contains subitems,can not be deleted.");
            }

            var actionResult = await this.menuService.DeleteAsync(id);

            var menuLanguages = await this.redisService.GetAsync<List<SysMenuLanguage>>(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName);
            var oldMenuLans = menuLanguages.Where(t => t.MenuId.Equals(id) && t.IsDel.Equals(0)).ToList();
            if (oldMenuLans.Count() > 0)
            {
                await this.menuLanguageService.DeleteRangeAsync(oldMenuLans.Select(t => t.Id).ToArray());

                menuLanguages.RemoveRange(oldMenuLans);
            }

            #region 数据一致性维护

            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);
            var menu = menus.Where(t => t.Id == id).FirstOrDefault();
            menus.Remove(menu);
            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResult);
        }

        #endregion

        #region 批量删除菜单信息

        /// <summary>
        /// 批量删除菜单信息
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("menu:del:entities")]
        public async Task<IActionResult> DeleteRange(String ids)
        {
            var array = StringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                if (await this.Check(item))
                {
                    return BadRequest("code contains subitems,can not be deleted.");
                }
            }

            var actionResult = await this.menuService.DeleteRangeAsync(array);

            var menuLanguages = await this.redisService.GetAsync<List<SysMenuLanguage>>(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName);
            foreach (var item in array)
            {
                var oldMenuLans = menuLanguages.Where(t => t.MenuId.Equals(item) && t.IsDel.Equals(0)).ToList();
                if (oldMenuLans.Count > 0)
                {
                    await this.menuLanguageService.DeleteRangeAsync(oldMenuLans.Select(t => t.Id).ToArray());

                    menuLanguages.RemoveRange(oldMenuLans);
                }
            }

            #region 数据一致性维护

            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);
            foreach (var item in array)
            {
                var menu = menus.Where(t => t.Id == item).FirstOrDefault();
                menus.Remove(menu);
            }
            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.redisService.SetAsync(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResult);
        }

        #endregion

        #region 数据删除检测

        /// <summary>
        /// 数据删除检测
        /// </summary>
        /// <param name="parentId">父键</param>
        /// <returns>真假值</returns>
        private async Task<bool> Check(Guid parentId)
        {
            var menus = await this.redisService.GetAsync<List<SysMenuDTO>>(GlobalContext.SystemCacheConfig.MenuCacheName);

            var subMenus = menus.Where(m => m.ParentId.Equals(parentId)).ToList();
            if (subMenus.Count > 0)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
