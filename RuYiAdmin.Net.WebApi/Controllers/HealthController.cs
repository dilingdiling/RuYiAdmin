﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.Controllers
{
    /// <summary>
    /// 健康检查控制器
    /// </summary>
    [AllowAnonymous]
    [Route(GlobalContext.RouteTemplate)]
    [Produces("application/json")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// 健康检查
        /// </summary>
        /// <returns>OkObjectResult</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() =>
            {
                return Ok("ok");
            });
        }
    }
}
